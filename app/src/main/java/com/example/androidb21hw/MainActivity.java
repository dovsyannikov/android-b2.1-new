package com.example.androidb21hw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText login;
    EditText email;
    EditText mobile;
    EditText password;
    EditText confirmpassword;

    TextView result;
    Button validate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = findViewById(R.id.login);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        password = findViewById(R.id.password);
        confirmpassword = findViewById(R.id.confirmPassword);

        result = findViewById(R.id.result);
        validate = findViewById(R.id.validate);

        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validation.validationResult(login, mobile, email, password, confirmpassword);
                result.setText(Validation.validationResult(login, mobile, email, password, confirmpassword));

//                Toast toast = Toast.makeText(MainActivity.this, Validation.validationResult(login, mobile, email, password, confirmpassword), Toast.LENGTH_LONG);
//                toast.show();
            }
        });
    }
}
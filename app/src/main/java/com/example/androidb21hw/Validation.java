package com.example.androidb21hw;

import android.widget.EditText;

public class Validation {
    public static String validateEmail(EditText text){
        String result = "Please fill your email, it can't be empty";
        String email = text.getText().toString();
        String pattern = "^[-a-z0-9~!$%^&*_=+}{\\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\\'?]+)*@([a-z0-9_][-a-z0-9_]*(\\.[-a-z0-9_]+" +
                ")*\\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\\" +
                ".[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?$";
        if (!email.equalsIgnoreCase("")){
            if (email.matches(pattern)){
                return result = "";
            }else {
                return result = "Please fill valid email";
            }
        }else {
            return result;
        }
    }
    public static String validateMobile(EditText text){
        String result = "Please fill your mobile, it can't be empty";
        String mobile = text.getText().toString();
        String pattern = "^\\+(?:[0-9] ?){6,14}[0-9]$";
        if (!mobile.equals("")){
            if (mobile.matches(pattern)){
                return result = "";
            }else {
                return result = "Please fill valid mobile";
            }
        }else {
            return result;
        }
    }
    public static String validateLogin(EditText text){
        String result = "Please fill your login, it can't be empty";
        String login = text.getText().toString();
        if (!login.equalsIgnoreCase("")){
            return result = "";
        }else {
            return result;
        }
    }
    public static String validatePasswordMatch(EditText password1, EditText password2){
        String result = "One or both of passwords fields are empty, please recheck";
        String pass1 = password1.getText().toString();
        String pass2 = password2.getText().toString();
        if (!pass1.equalsIgnoreCase("") && !pass2.equalsIgnoreCase("")){
            if (pass1.equals(pass2)){
                return result = "";
            } else {
                return result = "Passwords aren't the same";
            }

        }else {
            return result;
        }
    }
    public static StringBuilder validationResult(EditText login, EditText mobile, EditText email, EditText password, EditText confirmpassword){
        String newLine = System.getProperty("line.separator");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Validation.validateLogin(login));
        if (!validateLogin(login).equalsIgnoreCase("")){
            stringBuilder.append(newLine);
        }
        stringBuilder.append(Validation.validateMobile(mobile));
        if (!validateMobile(mobile).equalsIgnoreCase("")){
            stringBuilder.append(newLine);
        }
        stringBuilder.append(Validation.validateEmail(email));
        if (!Validation.validateEmail(email).equalsIgnoreCase("")){
            stringBuilder.append(newLine);
        }
        stringBuilder.append(Validation.validatePasswordMatch(password, confirmpassword));
        if (stringBuilder.toString().length() < 5){
            return stringBuilder.append("Validation passed");
        }else{
            return stringBuilder;
        }
    }


}
